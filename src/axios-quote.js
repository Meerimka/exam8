import axios from 'axios';

const instance = axios.create({
    baseURL:'https://quote-junusova.firebaseio.com/',
});

export default instance;