import React, {Component, Fragment} from 'react';
import axios from '../../axios-quote';
import QuoteForm from "../../components/QuoteForm/QuoteForm";


class NewQuote extends Component {

    addQuote = quote=>{
        axios.post( '/quote.json', quote).then(() =>{
            this.props.history.replace('/');
        });

    }
    render() {
        return (
            <Fragment>
                <h1>Submit new quote</h1>
                <QuoteForm onSubmit={this.addQuote}/>
            </Fragment>

        );
    }
}

export default NewQuote;