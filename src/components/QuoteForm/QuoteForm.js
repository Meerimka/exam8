import React, {Component} from 'react';
import {CATEGORIES} from "../../constance";
import './QuoteForm.css';
import {Button, Col, Form, FormGroup, Input, Label} from 'reactstrap';


class QuoteForm extends Component {
    constructor(props){
        super(props);
        if(props.quotes){
            this.state = {...props.quotes}
        }else{
            this.state= {
                category: Object.keys(CATEGORIES)[0],
                text:'',
                author: '',
            }
        }
    }


    valueChanged =(event) =>{
        const{name , value} = event.target;
        this.setState({[name]:value})
    };

    submitHandler = event=>{
        event.preventDefault();
       this.props.onSubmit({...this.state})
    }

    render() {
        return (
            <Form  onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Category</Label>
                    <Col sm={10}>
                        <Input type="select" name="category" id="category"
                               value={this.state.category} onChange={this.valueChanged}>
                            {Object.keys(CATEGORIES).map(categoryId =>(
                                <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="author" sm={2}>Author name:</Label>
                    <Col sm={10}>
                        <Input type="text" name="author"  id="author"
                        value={this.state.author} onChange={this.valueChanged}
                         />
                    </Col>
                </FormGroup>
                <FormGroup row>
                <Label for="category" sm={2}>Type quote text</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="text"  id="text"
                          value={this.state.text} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{size:10 , offset: 2}}>
                    <Button type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default QuoteForm;