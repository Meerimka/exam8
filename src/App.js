import React, {Component, Fragment} from 'react';
import {NavLink as RouterNavLink, Route, Switch} from 'react-router-dom';
import './App.css';
import NewQuote from "./containers/NewQuote/NewQuote";
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import AllQuotes from "./containers/AllQuotes/AllQuotes";
import EditQuote from "./containers/EditQuote/EditQuote";

class App extends Component {
  render() {
    return (
    <Fragment>
         <Navbar color="dark" light expand="md">
             <NavbarBrand color="light">Quotes Centrall</NavbarBrand>
             <NavbarToggler/>
             <Collapse isOpen navbar>
                 <Nav className="ml-auto" navbar>
                     <NavItem>
                         <NavLink tag={RouterNavLink} to="/" color="light" exact>Quotes</NavLink>
                     </NavItem>
                     <NavItem>
                         <NavLink tag={RouterNavLink} to="/add" color="light">New Quotes</NavLink>
                     </NavItem>
                 </Nav>
             </Collapse>
         </Navbar>
         <Container>
       <Switch>
           <Route path="/" exact component={AllQuotes}/>
           <Route path="/add" exact component={NewQuote}/>
           <Route path="/quotes/:id/edit" exact component={EditQuote}/>
           <Route path="/quotes/:categoryId" exact component={AllQuotes}/>
           <Route render={() => <h1>Not Found !</h1>}/>
       </Switch>
         </Container>
    </Fragment>
    );
  }
}

export default App;
