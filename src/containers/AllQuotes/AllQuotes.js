import React, {Component} from 'react';
import {Button, Col, Jumbotron, Nav, NavItem, NavLink, Row} from "reactstrap";
import  {NavLink as RouterNavLink } from 'react-router-dom';
import {CATEGORIES} from "../../constance";

import axios from  '../../axios-quote';

class AllQuotes extends Component {
    state ={
        quotes: null
    };

    loadQuotes (){
        let url = '/quote.json';

        const categoryId = this.props.match.params.categoryId;
        if(categoryId){
            url+=`?orderBy="category"&equalTo="${categoryId}"`;
        }
        axios.get(url).then(response => {
            if(response.data !==null){
                const quotes = Object.keys(response.data).map(id =>{
                    return {...response.data[id], id};
                });
                this.setState({quotes})
            }

        })

    }

    componentDidMount(){
            this.loadQuotes();
    };

    componentDidUpdate(prevProps){
       if(this.props.match.params.categoryId !== prevProps.match.params.categoryId){
           this.loadQuotes();
       }
    };
    remove=(id)=> {
        axios.delete(`quote/${id}.json`).then((response) => {
            console.log(response);
            this.props.history.replace('/');
        });
    };


    render() {
        let quotes = null;
         if(this.state.quotes) {
             quotes = this.state.quotes.map(quote => (
                 <Jumbotron key={quote.id}>
                     <p className="lead">{quote.text}</p>
                     <hr className="my-2"/>
                     <p>{quote.author}</p>
                     <p className="lead">
                         <NavLink tag={RouterNavLink} to={'/quotes/' + quote.id + '/edit'}>
                             <Button color="primary">Edit</Button>
                         </NavLink>
                         <span><Button color="primary" onClick={() => this.remove(quote.id)}>X</Button></span>
                     </p>
                 </Jumbotron>
             ))
         }else{
             quotes =  null;
         }
        return (
            <Row  style={{marginTop: "20px"}}>
                <Col sm={3}>
                    <h5>Please select a category: </h5>
                    <Nav vertical>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/" exact >All quotes</NavLink>
                        </NavItem>
                        {Object.keys(CATEGORIES).map(quoteId =>(
                            <NavItem key={quoteId}>
                                <NavLink
                                    tag={RouterNavLink}
                                    to={"/quotes/" + quoteId}
                                    exact
                                >
                                    {CATEGORIES[quoteId]}
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>
                <Col sm={9}>
                    {quotes}
                </Col>
            </Row>
        );
    }
}

export default AllQuotes;