import React, {Component, Fragment} from 'react';
import axios from '../../axios-quote';
import QuoteForm from "../../components/QuoteForm/QuoteForm";

class EditQuote extends Component {
    state={
        quotes: null,
    };

    componentDidMount(){
        const  id = this.props.match.params.id;
        axios.get(`/quote/${id}.json`).then(response=>{
                console.log(response);
            this.setState({quotes: response.data})
        })
    };

    editQuote = quote =>{
        const  id = this.props.match.params.id;
        axios.put(`/quote/${id}.json`, quote).then(() =>{
            this.props.history.replace('/');
        });

    };

    render() {
        let editbox = <QuoteForm
            onSubmit={this.editQuote}
            quotes={this.state.quotes}
        />;
        if(!this.state.quotes){
            editbox = <div>....</div>
        }
        return (
           <Fragment>
               <h1>Edit Quote</h1>
               {editbox}
           </Fragment>
        );
    }
}

export default EditQuote;